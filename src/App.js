import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'


import './App.css';

import Home from "./components/Home"
import List from "./components/List"

function App() {
  return (
    <div className="App">
      <Router>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/photo/">Photo</Link>
            </li>
          </ul>
        </nav>
        <header className="App-header">
          <Route path="/" exact component={Home} />
          <Route path="/photo" component={List} />
        </header>
      </Router>
    </div>
  );
}

export default App;
