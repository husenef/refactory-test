import React from 'react';
import axios from 'axios'

import {
    Row, Container, Col,
    Pagination, PaginationItem, PaginationLink
} from 'reactstrap'

class List extends React.Component {

    state = {
        data: {}, paginate: {}
    }

    componentDidMount() {
        axios.get("https://jsonplaceholder.typicode.com/photos").then(res => {
            // console.log("res photo", res)
            if (res.status === 200) {
                this.paginator(res.data, 1, 100)
                this.setState({ data: res.data })
            }
        })
    }

    paginator(items, currentpage, per_page_page) {

        let page = currentpage || 1,
            per_page = per_page_page || 10,
            offset = (page - 1) * per_page,

            paginatedItems = items.slice(offset).slice(0, per_page),
            total_pages = Math.ceil(items.length / per_page);
        this.setState({
            paginate: {
                page: page,
                per_page: per_page,
                pre_page: page - 1 ? page - 1 : null,
                next_page: (total_pages > page) ? page + 1 : null,
                total: items.length,
                total_pages: total_pages,
                data: paginatedItems
            }
        })
        // return {
        //     page: page,
        //     per_page: per_page,
        //     pre_page: page - 1 ? page - 1 : null,
        //     next_page: (total_pages > page) ? page + 1 : null,
        //     total: items.length,
        //     total_pages: total_pages,
        //     data: paginatedItems
        // };
    }

    mappingImage() {
        const { data, paginate } = this.state
        // let list =""
        // let dataMap = this.paginator(data, 1, 100)
        console.log("mapping", paginate)
        if (Object.keys(paginate).length > 0) {
            return paginate.data.map(e => {
                return (<Col md={4}>
                    <img src={e.thumbnailUrl} className="img-fluid" />
                    <h4>{e.title}</h4>
                </Col>)
            })
        } else {
            return <>No data</>
        }
    }

    render() {
        const { data, paginate } = this.state
        // console.log([data, paginate])
        return (<>

            <Container>
                <Row>
                    <Col md={12}>
                        <Pagination aria-label="Page navigation example">
                            <PaginationItem>
                                <PaginationLink first href="#" onClick={(e) => this.paginator(data, 1, 100)} />
                            </PaginationItem>
                            {(paginate.pre_page !== null) ?
                                <PaginationItem>
                                    <PaginationLink previous href="#" onClick={(e) => this.paginator(data, paginate.pre_page, 100)} />
                                </PaginationItem> : ""}
                            {(paginate.next_page) ? <PaginationItem>
                                <PaginationLink next href="#" onClick={(e) => this.paginator(data, paginate.next_page, 100)} />
                            </PaginationItem> : ""}

                            <PaginationItem>
                                <PaginationLink last href="#" onClick={(e) => this.paginator(data, paginate.total_pages, 100)} />
                            </PaginationItem>
                        </Pagination>
                    </Col>
                </Row>
                <Row>
                    {this.mappingImage()}
                </Row>
            </Container>
        </>)
    }
}

export default List